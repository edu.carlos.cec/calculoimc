# Calculate BMI [English]

> Status: Developing ⚠️

### It is a mobile application planned by us, where we perform the calculation of BMI.

## Fields required to calculate, also they are validated.
+ Weight
+ Height

## Technologies used:
+ Kotlin


# Calculadora de IMC [Português Brasil]

> Status: Em desenvolvimento ⚠️

### Essa é uma aplicação mobile planejada por nós, na qual performamos  o cálculo do IMC.

## Campos necessários para o cálculo, eles também estão validados.
+ Peso
+ Altura

## Tecnologias usadas:
+ Kotlin


## Alguns links interessantes para aqueles que querem fazer a mesma aplicação por si mesmo.
+ [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
+ [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

## Documentação:
#Para ajudar a começar no Flutter, veja nossa:
+ [Documentação Oficial Online](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
