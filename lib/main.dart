import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

//Precisamos de um widget interativo, ele precisa mudar o seu estado
class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  //Chave global
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  //Informações mostradas na tela
  String _dataOutput = "Informe seus dados";

  //Controladores para podemos pegar os dados de um input
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();

  //Função para o botão de reset, acessando os controladores de cada input
  void _resetFields() {
    //Precisa rodar de novo o build
    setState(() {
      weightController.text = "";
      heightController.text = "";
      _dataOutput = "Informe seus dados";
    });
  }

  void _calcularImc() {
    setState(() {
      //Validar os dados
      double peso = double.parse(weightController.text);
      double altura = double.parse(heightController.text) / 100;
      double imc = peso / (altura * altura);
      print(imc);
      if (imc < 18.5) {
        _dataOutput = "Magreza (${imc.toStringAsPrecision(4)})";
      } else if(imc <= 24.9) {
        _dataOutput = "Peso normal (${imc.toStringAsPrecision(4)})";
      } else if(imc <= 29.9) {
        _dataOutput = "Sobrepeso (${imc.toStringAsPrecision(4)})";
      } else if(imc <= 34.9) {
        _dataOutput = "Obesidade de grau I (${imc.toStringAsPrecision(4)})";
      } else if(imc <= 39.9){
        _dataOutput = "Obesidade de grau II (${imc.toStringAsPrecision(4)})";
      } else {
        _dataOutput = "Obesidade de grau III (${imc.toStringAsPrecision(4)})";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Calculadora de IMC"),
          centerTitle: true, //Centralizar o título
          backgroundColor: Colors.green,
          actions: [
            IconButton(onPressed: _resetFields, icon: Icon(Icons.refresh))
          ],
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Icon(Icons.person_outline, size: 120.0, color: Colors.green),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: "Peso (kg)",
                      labelStyle: TextStyle(color: Colors.green),
                    ),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.green, fontSize: 25),
                    controller: weightController,
                    validator: (value){
                      //Pode ser null
                      if(value!.isEmpty){
                        return "Insira o seu peso!";
                      }
                    },
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: "Altura (kg)",
                      labelStyle: TextStyle(color: Colors.green),
                    ),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.green, fontSize: 25),
                    controller: heightController,
                    validator: (value){
                      //Pode ser null
                      if(value!.isEmpty){
                        return "Insira a sua altura!";
                      }
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 10.0,
                      bottom: 10.0,
                    ),
                    child: Container(
                      height: 50.0,
                      child: RaisedButton(
                        onPressed: (){
                          if(_formKey.currentState!.validate()){
                            _calcularImc();
                          }
                        },
                        child: Text(
                          "Calcular",
                          style: TextStyle(color: Colors.white, fontSize: 25),
                        ),
                        color: Colors.green,
                      ),
                    ),
                  ),
                  Text(
                    _dataOutput,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.green, fontSize: 25.0),
                  )
                ],
              ),
            )
        )
    );
  }
}
